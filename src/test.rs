#[test]
#[ignore]
fn turning_left_from_west_points_the_robot_south() {
    let robot = Robot::new(0, 0, Direction::West).RotateLeft();
    assert_eq!(&Direction::South, robot.direction());
}