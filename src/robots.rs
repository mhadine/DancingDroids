 // X_max Y_max
 // position du robot 2 en x=1 y=1 orientation=nord
 // Liste d'instruction du robot

// position du robot 2 x=3 y=2 orientation=South


// Hadine Marouane
// num d'etudiant :17805157
// groupe L2-A





use crate::Direction::{North, East, West, South};
use std::alloc::dealloc;
use crate::Command::{L, F, R};

// j' ai utilisé enum command qui presente les movements du robot : 
// F = "forward"
// L = "left"
// R = "right"

enum Command{
    F,
    L,                          
    R
}


// enum diresction presente les directions north,south,east,west
enum Direction{
    East,
    West,
    North,
    South
}


// RobotPosition is a class qui contienne :
// x position de robot dans l axe des abcisses 
// y position de robot dans l axe des ordonnes 
// xmax la valeur maximum de x 
// ymax la valeur maximum de y 
//   on peut dire que xmax et ymax les bornes du terrain ou le robot se presente  

struct RobotPosition{
    x: i32,
    x_max: i32,

    y: i32,
    y_max: i32,

    direction: Direction,
    // direction du robot comme c'est montré dans l enum direction 
}

impl RobotPosition{
    pub fn new(x: i32, x_max: i32, y: i32, y_max: i32, direction: Direction) -> Self {
        Self {
            x,  // normalement x a une valeur de 0 par defaut 
            x_max: 5,
            y,  // normalement y a une valeur de 0 par defaut
            y_max: 5,
            direction:North
        }
             // en generale on trace les bornes du terrain d'une maniere un peu fix mais le robot il peut etre a n importe quel


                            /*  5  .  .  .  .  .  .
                                4  .  .  .  .  .  .
                                3  .  .  .  .  .  .
                                2  .  .  .  .  .  .
                                1  .  .  .  .  .  .
                                0  .  .  .  .  .  .
                                   
                                   0  1  2  3  4  5
                            */
        }

    // cette fonction s'occupe des mouvements du robot 
        pub fn move_robot(&mut self, x: i32, y: i32, direction: Direction) {
        self.x += x; // pour x 
        self.y += y; // pour y
        self.direction = direction;
    }

    pub fn setX(&mut self, x: i32){
        //check validity of position
        self.x += x;
    }
    pub fn setY(&mut self, y: i32){
        self.y += y;
    }
    pub fn changeDirection(&mut self, direction: Direction){
        self.direction = direction;
    }
}


// et voila notre class robot 
struct Robot{
    position: RobotPosition,
    //commands: [Command],
}

impl Robot {
    pub fn new(position: RobotPosition) -> Self {
        Self {
            position,

        }
    }

                                                                        
// premierement je me suis inspire de la boussole              
                    
                    //      N
                    //  W       E
                    //      S       
//  j ai pense a faire des rotation a partir dune direction :
//  par exemple dans cette fn RotateLeft si la rotation est vers l agauche  et on est dans le nord,
//  donc tout simplement avec la rotationleft la nouvelle direction ca doit etre west                                                                   
        

    pub fn RotateLeft(&mut self){
        if self.position.direction == North{
            self.position.direction = West;
        }
        if self.position.direction == West{
            self.position.direction = South;
        }
        if self.position.direction.cmp(South{
            self.position.direction = East;
        }
        if self.position.direction == East{
            self.position.direction = North;
        }
    }
// la meme chose pour la rotationRight 
    pub fn RotateRight(&mut self){
        if self.position.direction == North{
            self.position.direction = East;
        }
        if self.position.direction == West{
            self.position.direction = North;
        }
        if self.position.direction == South{
            self.position.direction = West;
        }
        if self.position.direction == East{
            self.position.direction = South;
        }
    }
// alors la c pour avancer le robot et la fonction fn MoveForward qui s'occupe de ce travail 
// n'oubliant pas que au debut on a construit mun terrain et des bornes ou le robot va ce bouger et acter
// donc certains moves can not be possible or invalids moves.

    pub fn MoveForward(&mut self){
        if self.position.direction == North{
            if self.position.y > self.y_max {
                panic!("invalid move");
            // dans notre cas ymax est 5 donc ca peut jamais y depasser ymax
            // dans la direction du nord 
            }
            self.position.y = self.position.y + 1;
        }
        if self.position.direction == West{
            if self.position.x <= 0 {
                panic!("invalid move");
            // pour la direction  west la position on x ne doit pas passer 0 
            // car 0 c la borne 
            }
            self.position.x = self.position.x - 1;

        }
        if self.position.direction == South{
            if self.position.y <= 0 {
                panic!("invalid move");
            // le meme logique pour sud
            }
            self.position.y = self.position.y - 1;
        }
        if self.position.direction == East{
            if self.position.x > self.x_max {
                panic!("invalid move");
            // pareil pour east
            }
            self.position.x = self.position.x + 1;
        }
    }

    // la fonction executeMovements prend un tableau de commande se forme 
    // de "LLFRFLLF" et se transforme les caracteres d'enum command declaree en haut et les rendres 
    // des mouvements comme L = Left.....
    pub fn executeMovements(&mut self, arr: &[Command]){
        for command in arr {
            if command == L{
                self.RotateLeft();
            }
            if command == R{
                self.RotateRight();
            }
            if command == F{
                self.MoveForward();
            }
        }
    }

    // la fonction stringToCommands change le string et le rendre des commandes.
    pub fn stringToCommands(&mut self, commands: String){
        for c in commands.chars() {
            if c == 'L' {
                self.RotateLeft();
            }
            if c == 'R' {
                self.RotateRight();
            }
            if c == 'F' {
                //check collision
                self.MoveForward();
            }
        }
    }

}


fn main() {
    let commands = "LLFRFLLF";
    let robot2 = new.robot;
}
